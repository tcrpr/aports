# Contributor: Stefan Wagner <stw@bit-strickerei.de>
# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=py3-kombu
pkgver=4.6.9
pkgrel=0
pkgdesc="a message queue abstraction layer"
options="!check" # 3 Redis tests fail
url="https://pypi.python.org/pypi/kombu/"
arch="noarch !s390x" # Limited by py3-dill
license="BSD-3-Clause"
depends="py3-amqp py3-vine py3-importlib-metadata"
makedepends="py3-setuptools"
checkdepends="py3-pyro4 py3-case py3-nose py3-mock py3-tz py3-pytest py3-sqlalchemy py3-fakeredis"
source="https://files.pythonhosted.org/packages/source/k/kombu/kombu-$pkgver.tar.gz"
builddir="$srcdir/kombu-$pkgver"

replaces="py-kombu" # Backwards compatibility
provides="py-kombu=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}
sha512sums="39c1936eb1fb5b66fc727220cb19f781ed43d4f5997d33d0ad7ebb1e06efde33a0c3f93a4d6e91385515d4e693040a487647e25de669963a3e83aafa5ef18dbb  kombu-4.6.9.tar.gz"
