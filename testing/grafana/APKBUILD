# Contributor: Konstantin Kulikov <k.kulikov2@gmail.com>
# Maintainer: Konstantin Kulikov <k.kulikov2@gmail.com>
pkgname=grafana
pkgver=7.0.1
pkgrel=0
_commit=ef5b586d7d # git rev-parse --short HEAD
_stamp=1590480356 # git --no-pager show -s --format=%ct
pkgdesc="Open source, feature rich metrics dashboard and graph editor"
url="https://grafana.com"
arch="all"
license="Apache-2.0"
makedepends="go"
install="$pkgname.pre-install"
subpackages="$pkgname-openrc"
options="net chmod-clean"
source="$pkgname-$pkgver.tar.gz::https://github.com/grafana/grafana/archive/v$pkgver.tar.gz
	$pkgname-$pkgver-bin.tar.gz::https://dl.grafana.com/oss/release/grafana-$pkgver.linux-amd64.tar.gz
	$pkgname.initd $pkgname.confd"

export GOPATH=${GOPATH:-$srcdir/go}
export GOCACHE=${GOCACHE:-$srcdir/go-build}
export GOTMPDIR=${GOTMPDIR:-$srcdir}

# secfixes:
#   6.3.4-r0:
#     - CVE-2019-15043

build() {
	local ldflags="-X main.version=$pkgver -X main.commit=$_commit -X main.buildstamp=$_stamp"
	go build -ldflags "$ldflags" -v github.com/grafana/grafana/pkg/cmd/grafana-server
	go build -ldflags "$ldflags" -v github.com/grafana/grafana/pkg/cmd/grafana-cli
}

check() {
	local pkgs="./..."
	case "$CARCH" in
	# Out of bounds test failure on 32bit archs, likely bug in test.
	# https://github.com/grafana/grafana/issues/25287
	armhf|armv7|x86) pkgs="$(go list ./... | grep -Ev '(pkg/tsdb$)|(pkg/services/alerting/conditions$)')" ;;
	esac

	go test $pkgs
}

package() {
	install -Dm755 "$srcdir/$pkgname.initd" "$pkgdir/etc/init.d/$pkgname"
	install -Dm644 "$srcdir/$pkgname.confd" "$pkgdir/etc/conf.d/$pkgname"
	install -Dm755 "$builddir/$pkgname-server" "$pkgdir/usr/sbin/$pkgname-server"
	install -Dm755 "$builddir/$pkgname-cli" "$pkgdir/usr/bin/$pkgname-cli"
	install -Dm644 "$builddir/conf/sample.ini" "$pkgdir/etc/grafana.ini"
	install -dm755 "$pkgdir/usr/share/grafana"
	cp -r "$builddir/conf" "$builddir/public" "$pkgdir/usr/share/$pkgname/"
}

sha512sums="c3d7563e702643588fd7f971b3c1cca51c350278901464053ea9ccc5a9f1a04e96f8b77ccb70367ec4d3ef144a3b10862874ba2cf8c2c17b5dcd2a060f8d5dde  grafana-7.0.1.tar.gz
8304096aff864aa120b1de753c219b237f678d14a1457a11bb0a762a447db59755a5727cbedba35ac7823d427ece03b3bb9eeba98bc80b76a7484114101429f9  grafana-7.0.1-bin.tar.gz
b0a781e1b1e33741a97e231c761b1200239c6f1235ffbe82311fe883387eb23bef262ad68256ebd6cf87d74298041b53b947ea7a493cfa5aa814b2a1c5181e13  grafana.initd
c2d9896ae9a9425f759a47aeab42b7c43b63328e82670d50185de8c08cda7b8df264c8b105c5c3138b90dd46e86598b16826457eb3b2979a899b3a508cbe4e8c  grafana.confd"
